<?php

namespace App\Services;

use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use App\Repositories\FileRepo;


/**
 * Class FileServ
 *
 * @package namespace App\Services;
 */
class FileServ
{


    public function __construct()
    {

        $this->fileRepo = new FileRepo();
    } // END function


    public function moveFileTo($filename, $target, $source = '')
    {

        $fileDatum = $this->findByFilename($filename);

        if ($fileDatum->isEmpty()) {
            return false;
        } // END if

        $sizes = config('image.sizes');
        $sizes['o'] = 0;
        $fileParentType = $fileDatum->first()->parent_type;
        $fileExtension = $fileDatum->first()->extension;

        $source .= empty($source) ? '' : '/';

        foreach ($sizes as $id => $size) {

            $file = $filename . '_' . $id . '.' . $fileExtension;

            $exists = Storage::disk('gallery')->exists($source . $file);

            if (empty($exists)) {continue;}

            Storage::disk('gallery')->move($source . $file, $target . '/'. $file);
        } // END foreach

        return true;
    } // END function


    /**
     * Covert image
     *
     * @param  $parentType
     * @param  \Illuminate\Filesystem\Filesystem  $file
     *
     * @throws
     * @return string   $filename
     */
    public function convertImage($parentType, $file, $extension = null)
    {

        $sizes = config('image.sizes');
        $savePath  = public_path('gallery/' . $parentType);

        $filename = Carbon::now()->timestamp . mt_rand(100000, 999999);
        $fileExtension = empty($extension) ? $file->extension() : $extension;

        if (!empty($extension)) {
            $convertFile = file_get_contents($file);

            if ($convertFile === FALSE) {
                return false;
            } // END if
        } else {
            $convertFile = $file;
        } // END if

        $img = Image::make($convertFile);

        $filePath = $savePath . '/' . $filename . '_o.' . $fileExtension;
        $img->save($filePath);

        $imgWidth = $img->width();

        foreach ($sizes as $id => $size) {
            if ($imgWidth < $size['width']) {
                continue;
            } // END if

            if ($size['width'] == $size['height']) {
                $img->fit($size['width']);
            } else {
                $img->resize($size['width'], $size['height'], function ($constraint) {
                    $constraint->aspectRatio();
                });
            } // END if else

            $filePath = $savePath . '/' . $filename . '_' . $id . '.' . $fileExtension;
            $img->save($filePath);
        } // END foreach

        return $filename;
    } // END function


    public function findLinks($filename)
    {

        $fileDatum = $this->findByFilename($filename);

        if ($fileDatum->isEmpty()) {
            return [];
        } // END if

        $links = [];
        $sizes = config('image.sizes');
        $sizes['o'] = 0;
        $parentType = $fileDatum->first()->parent_type;
        $parentId = intval($fileDatum->first()->parent_id);
        $filename = $fileDatum->first()->filename;
        $fileExtension = $fileDatum->first()->extension;

        $path  = $parentType .'/';
        $path .= empty($parentId) ? '' : $parentId . '/';

        foreach ($sizes as $id => $size) {
            $file =  $filename . '_' . $id . '.' . $fileExtension;

            $exists = Storage::disk('gallery')->exists($path . $file);
            if (empty($exists)) {continue;}

            $links[$id] = 'gallery/' . $path . $file;
        } // END foreach


        return $links;
    } // END function


    /*
     * delete
     *
     * @param $where
     *
     * @return
     */
    public function delete($where = [])
    {
        if (!array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->fileRepo->deleteData($where);
    } // END function


    /*
     * update
     *
     * @param $data
     * @param $where
     *
     * @return
     */
    public function update($data, $where = [])
    {
        if (!is_array($data) OR !array_key_exists('id', $where)) {
            return false;
        } // END if

        return $this->fileRepo->updateData($data, $where);
    } // END function


    /*
     * create
     *
     * @param $parentType
     * @param $parentId
     * @param $sessionId
     * @param $filename
     * @param $extension
     * @param $mimeType
     * @param $size
     * @param $type
     * @param $status
     *
     * @return
     */
    public function create($parentType, $parentId, $sessionId, $filename, $extension, $mimeType, $size, $type = 'image', $status = 'enable')
    {
        $data = ['parent_type' => $parentType,
                 'parent_id'   => $parentId,
                 'session_id' => $sessionId,
                 'filename'  => $filename,
                 'extension' => $extension,
                 'mime_type' => $mimeType,
                 'size'   => $size,
                 'type'   => $type,
                 'status' => $status
        ];

        return $this->fileRepo->createData($data);
    } // END function


    /*
     * findByParentTypeAndParentId
     *
     * @param $parentType
     * @param $parentId
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByParentTypeAndParentId($parentType, $parentId, $status = '', $orderby = [], $page = -1, $numItems = 10)
    {
        $where = ['parent_type' => $parentType, 'parent_id' => $parentId];

        if (!empty($status)) {
            $where['status'] = $status;
        } // END if

        return $this->fileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByStatus
     *
     * @param $status
     * @param $orderby
     * @param $page
     * @param $numItems
     *
     * @return
     */
    public function findByStatus($status, $orderby = [], $page = -1, $numItems = 10)
    {
        $where = ['status' => $status];

        return $this->fileRepo->fetchData($where, $orderby, $page, $numItems);
    } // END function


    /*
     * findByFilename
     *
     * @param $filename
     *
     * @return
     */
    public function findByFilename($filename)
    {
        $where = ['filename' => $filename];

        return $this->fileRepo->fetchDatum($where);
    } // END function


    /*
     * findById
     *
     * @param $id
     *
     * @return
     */
    public function findById($id)
    {
        $where = ['id' => $id];

        return $this->fileRepo->fetchDatum($where);
    } // END function

}
