<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(env('CROS_DISABLE', false) == true) {
            $response = $next($request);
            return $response;
        }

        $headers = [
            'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE, HEAD',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => $request->header('Access-Control-Request-Headers'),
        ];


        $allowHeaders = explode(',', $headers['Access-Control-Allow-Headers']);

        foreach ($allowHeaders as $allowHeader) {
            $headers[$allowHeader] = $request->header($allowHeader);
        } // END foreach


        if ($request->isMethod('OPTIONS')) {
            $response = response('', 200, $headers);
        } else {
            $response = $next($request);

            foreach ($headers as $key => $value) {
                $response->header($key, $value);
            } // END foreach
        } // END if else


        return $response;
    } // END function
}
