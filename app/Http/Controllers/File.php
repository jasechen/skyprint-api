<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\FileServ;


class File extends Controller
{

    /**
     *
     */
    public function __construct()
    {

        $this->sessionServ   = app(SessionServ::class);
        $this->fileServ = app(FileServ::class);
    } // END function

    /**
     * Upload the file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function upload(Request $request)
    {

        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($request->hasFile('file'))) {
            $code = 400;
            $comment = 'file empty';

            Jsonponse::fail($comment, $code);
        } // END if
        $status = $request->input('status', 'upload');
        $isAlive = $this->sessionServ->isAlive($sessionCode);
        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';
            Jsonponse::fail($comment, $code);
        } // END if

        $sessionDatum = $this->sessionServ->findByCode($sessionCode);
        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:' . implode(',', config('tbl_files.FILES_STATUS'))]]
        );

        if ($statusValidator->fails()) {
            $code = 422;
            $comment = 'status error';

            Jsonponse::fail($comment, $code);
        } // END if

        $fileUpload = $request->file('file');

        if (empty($fileUpload->isValid())) {
            $code = 422;
            $comment = 'file error';

            Jsonponse::fail($comment, $code);
        } // END if

        echo 1;
        exit;

        $fileExtension = $fileUpload->extension();
        $fileMimeType  = $fileUpload->getMimeType();
        $fileSize      = $fileUpload->getSize();
        $fileOriginalName = $fileUpload->getClientOriginalName();

        $fileMimeTypes = explode('/', $fileMimeType);
        foreach (['image', 'audio', 'video'] as $defaultFileType) {
            if ($fileMimeTypes[0] == $defaultFileType) {
                $fileType = $defaultFileType;
                break;
            } // END if
        } // END foreach

        $fileType = in_array($fileType, ['image', 'audio', 'video']) ? $fileType : 'others';


        $filename = $this->fileServ->convertImage($parentType, $fileUpload);


        $fileDatum = $this->fileServ->create($parentType, 0, $sessionDatum->first()->id, $filename, $fileExtension, $fileMimeType, $fileSize, $fileType, $status);

        if ($fileDatum->isEmpty()) {
            $code = 500;
            $comment = 'create error';

            Jsonponse::fail($comment, $code);
        } // END if


        $fileLinks = $this->fileServ->findLinks($filename);


        $resultData = ['session'  => $sessionCode, 'file_id'  => $fileDatum->first()->id, 'filename' => $fileDatum->first()->filename, 'file_links' => $fileLinks];

        Jsonponse::success('upload success', $resultData, 201);
    } // END function


} // END class
