<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Package\Jsonponse\Jsonponse;

use App\Services\SessionServ;
use App\Services\CategoryServ;
use App\Services\CategorySeoServ;
use App\Services\AuthorServ;
use App\Services\PostServ;
use App\Services\PostSeoServ;
use App\Services\FileServ;

class Tool extends Controller
{

    /**
     *
     */
    public function __construct()
    {
        $this->sessionServ  = app(SessionServ::class);
        $this->categoryServ = app(CategoryServ::class);
        $this->categorySeoServ = app(CategorySeoServ::class);
        $this->authorServ = app(AuthorServ::class);
        $this->postServ = app(PostServ::class);
        $this->postSeoServ = app(PostSeoServ::class);
        $this->fileServ = app(FileServ::class);
    } // END function


    /**
     * make2Html
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function makeCategory2Html(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum->first()->site_name = 'English.Agency';
        $categoryDatum->first()->url_path = '/' . $categoryDatum->first()->slug . '-c' . $categoryDatum->first()->id;

        $categoryDatum->first()->cover_links = [];
        if (!empty($categoryDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($categoryDatum->first()->cover);
            $categoryDatum->first()->cover_links = $coverLinkData;
        } // END if else

        $categoryDatum->first()->recommend_posts = [];
        $postData = $this->postServ->findByCategoryIdAndRecommended($categoryDatum->first()->id, 'publish', ['p.id' => 'DESC']);
        if ($postData->isNotEmpty()) {
            foreach($postData->all() as $postDatum) {

                $postDatum->cover_links = [];
                if (!empty($postDatum->cover)) {
                    $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                    $postDatum->cover_links = $coverLinkData;
                } // END if else

                $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                $categoryDatum->first()->recommend_posts[] = get_object_vars($postDatum);
            }
        } // END if


        $tHtmlPath = 'category/' . $categoryDatum->first()->status . '/';
        $htmlFile  = $categoryDatum->first()->id . '.html';

        $tExists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);
        if (!empty($tExists)) {
            Storage::disk('public')->delete($tHtmlPath . $htmlFile);
        } // END if


        $inputData = get_object_vars($categoryDatum->first());
        $content = view('category', $inputData)->__toString();

        Storage::disk('public')->put($tHtmlPath . $htmlFile, $content);


        $exists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);

        if (empty($exists)) {
            $code = 404;
            $comment = 'html file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $this->categoryServ->update(['html_made' => true], ['id' => $id]);


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('make success', $resultData, 202);
    } // END function


    /**
     * syncHtml
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function syncCategoryHtml(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum = $this->categoryServ->findById($id);

        if ($categoryDatum->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $oHtmlPath = 'public/category/' . $categoryDatum->first()->status . '/';
        $tHtmlPath = 'sync/category/';
        $htmlFile = $categoryDatum->first()->id . '.html';


        if ($categoryDatum->first()->status == config('tbl_categories.CATEGORIES_STATUS_ENABLE')) {

            if (empty($categoryDatum->first()->html_made)) {
                $code = 422;
                $comment = 'html_made error';

                Jsonponse::fail($comment, $code);
            } // END if

            $oExists = Storage::disk('local')->exists($oHtmlPath . $htmlFile);
            if (!empty($oExists)) {
                $tExists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);
                if (!empty($tExists)) {
                    Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                } // END if

                Storage::disk('local')->copy($oHtmlPath . $htmlFile, $tHtmlPath . $htmlFile);
            } // END if
        } // END if

        if ($categoryDatum->first()->status != config('tbl_categories.CATEGORIES_STATUS_ENABLE')) {

            $exists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);

            if (!empty($exists)) {
                Storage::disk('local')->delete($tHtmlPath . $htmlFile);
            } // END if
        } // END if


        $this->categoryServ->update(['synced' => true], ['id' => $id]);


        $this->_writeData2Htaccess();


        $resultData = ['session' => $sessionCode, 'category_id' => $id];

        Jsonponse::success('sync success', $resultData, 202);
    } // END function


    /**
     * make2Html
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function makeCategoryAll2Html(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $status = config('tbl_categories.CATEGORIES_STATUS_ENABLE');
        $categoryData = $this->categoryServ->findAll($status);

        if ($categoryData->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $successCategoryIds = $failCategoryIds = [];
        foreach ($categoryData->all() as $categoryDatum) {

            $categoryDatum->site_name = 'English.Agency';
            $categoryDatum->url_path = '/' . $categoryDatum->slug . '-c' . $categoryDatum->id;

            $categoryDatum->cover_links = [];
            if (!empty($categoryDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($categoryDatum->cover);
                $categoryDatum->cover_links = $coverLinkData;
            } // END if else

            $categoryDatum->recommend_posts = [];
            $postData = $this->postServ->findByCategoryIdAndRecommended($categoryDatum->id, 'publish', ['p.id' => 'DESC']);
            if ($postData->isNotEmpty()) {
                foreach($postData->all() as $postDatum) {

                    $postDatum->cover_links = [];
                    if (!empty($postDatum->cover)) {
                        $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                        $postDatum->cover_links = $coverLinkData;
                    } // END if else

                    $postDatum->url_path = empty($postDatum->slug) ? '' : '/' . $postDatum->slug . '-p' . $postDatum->id;

                    $categoryDatum->recommend_posts[] = get_object_vars($postDatum);
                }
            } // END if


            $tHtmlPath = 'category/' . $categoryDatum->status . '/';
            $htmlFile  = $categoryDatum->id . '.html';

            $tExists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);
            if (!empty($tExists)) {
                Storage::disk('public')->delete($tHtmlPath . $htmlFile);
            } // END if


            $inputData = get_object_vars($categoryDatum);
            $content = view('category', $inputData)->__toString();

            Storage::disk('public')->put($tHtmlPath . $htmlFile, $content);


            $exists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);

            if (empty($exists)) {
                $failCategoryIds[] = $categoryDatum->id;
            } else {
                $this->categoryServ->update(['html_made' => true], ['id' => $categoryDatum->id]);
                $successCategoryIds[] = $categoryDatum->id;
            } // END if

        } // END foreach


        if (!empty($failCategoryIds)) {
            $code = 404;
            $comment = 'html file error';
            $data['fail_category_ids'] = $failCategoryIds;

            Jsonponse::fail($comment, $code, $data);
        } // END if


        $resultData = ['session' => $sessionCode, 'success_category_ids' => $successCategoryIds];

        Jsonponse::success('make success', $resultData, 202);
    } // END function


    /**
     * syncHtml
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function syncCategoryAllHtml(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryData = $this->categoryServ->findAll();

        if ($categoryData->isEmpty()) {
            $code = 404;
            $comment = 'category error';

            Jsonponse::fail($comment, $code);
        } // END if


        $successCategoryIds = $failCategoryIds = [];
        foreach ($categoryData->all() as $categoryDatum) {

            $oHtmlPath = 'public/category/' . $categoryDatum->status . '/';
            $tHtmlPath = 'sync/category/';
            $htmlFile = $categoryDatum->id . '.html';


            if ($categoryDatum->status == config('tbl_categories.CATEGORIES_STATUS_ENABLE')) {

                if (empty($categoryDatum->html_made)) {
                    $failCategoryIds[] = $categoryDatum->id;
                } else {
                    $oExists = Storage::disk('local')->exists($oHtmlPath . $htmlFile);
                    if (!empty($oExists)) {
                        $tExists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);
                        if (!empty($tExists)) {
                            Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                        } // END if

                        Storage::disk('local')->copy($oHtmlPath . $htmlFile, $tHtmlPath . $htmlFile);
                    } // END if
                } // END if
            } // END if

            if ($categoryDatum->status != config('tbl_categories.CATEGORIES_STATUS_ENABLE')) {

                $exists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);

                if (!empty($exists)) {
                    Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                } // END if
            } // END if

            if (!empty($categoryDatum->html_made)) {
                $this->categoryServ->update(['synced' => true], ['id' => $categoryDatum->id]);
                $successCategoryIds[] = $categoryDatum->id;
            } // END id
        } // END foreach


        if (!empty($failCategoryIds)) {
            $code = 422;
            $comment = 'html_made error';
            $data['fail_category_ids'] = $failCategoryIds;

            Jsonponse::fail($comment, $code, $data);
        } // END if


        $this->_writeData2Htaccess();


        $resultData = ['session' => $sessionCode, 'success_category_ids' => $successCategoryIds];

        Jsonponse::success('sync success', $resultData, 202);
    } // END function


    /**
     * make2Html
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function makePost2Html(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $categoryDatum = $this->categoryServ->findById($postDatum->first()->category_id);
        $categoryDatum->first()->url_path = '/' . $categoryDatum->first()->slug . '-c' . $categoryDatum->first()->id;

        $categoryDatum->first()->cover_links = [];
        if (!empty($categoryDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($categoryDatum->first()->cover);
            $categoryDatum->first()->cover_links = $coverLinkData;
        } // END if else

        $postDatum->first()->category = get_object_vars($categoryDatum->first());


        $authorDatum = $this->authorServ->findById($postDatum->first()->author_id);
        $authorDatum->first()->avatar_links = [];
        if (!empty($authorDatum->first()->avatar)) {
            $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
            $authorDatum->first()->avatar_links = $avayarLinkData;
        } // END if else

        $postDatum->first()->author = get_object_vars($authorDatum->first());


        $postDatum->first()->url_path = '/' . $postDatum->first()->slug . '-p' . $postDatum->first()->id;
        $postDatum->first()->site_name = 'English.Agency';
        $postDatum->first()->cover_links = [];
        if (!empty($postDatum->first()->cover)) {
            $coverLinkData = $this->fileServ->findLinks($postDatum->first()->cover);
            $postDatum->first()->cover_links = $coverLinkData;
        } // END if else

        if (empty($postDatum->first()->canonical_url)) {
            $postDatum->first()->canonical_url = env('WEB_URL') . $postDatum->first()->url_path;
        } // END if

        $tHtmlPath = 'post/' . $postDatum->first()->status . '/';
        $htmlFile  = $postDatum->first()->id . '.html';

        $tExists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);
        if (!empty($tExists)) {
            Storage::disk('public')->delete($tHtmlPath . $htmlFile);
        } // END if


        $inputData = get_object_vars($postDatum->first());
        $content = view('post', $inputData)->__toString();

        Storage::disk('public')->put($tHtmlPath . $htmlFile, $content);


        $exists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);

        if (empty($exists)) {
            $code = 404;
            $comment = 'html file error';

            Jsonponse::fail($comment, $code);
        } // END if


        $this->postServ->update(['html_made' => true], ['id' => $id]);


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('make success', $resultData, 202);
    } // END function


    /**
     * syncHtml
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function syncPostHtml(Request $request, $id)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $postDatum = $this->postServ->findById($id);

        if ($postDatum->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $oHtmlPath = 'public/post/' . $postDatum->first()->status . '/';
        $tHtmlPath = 'sync/post/';
        $htmlFile = $postDatum->first()->id . '.html';


        if ($postDatum->first()->status == config('tbl_posts.POSTS_STATUS_PUBLISH')) {

            if (empty($postDatum->first()->html_made)) {
                $code = 422;
                $comment = 'html_made error';

                Jsonponse::fail($comment, $code);
            } // END if

            $oExists = Storage::disk('local')->exists($oHtmlPath . $htmlFile);
            if (!empty($oExists)) {
                $tExists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);
                if (!empty($tExists)) {
                    Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                } // END if

                Storage::disk('local')->copy($oHtmlPath . $htmlFile, $tHtmlPath . $htmlFile);
            } // END if
        } // END if

        if ($postDatum->first()->status != config('tbl_posts.POSTS_STATUS_PUBLISH')) {

            $exists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);

            if (!empty($exists)) {
                Storage::disk('local')->delete($tHtmlPath . $htmlFile);
            } // END if
        } // END if


        $this->postServ->update(['synced' => true], ['id' => $id]);


        $this->_writeData2Htaccess();


        $resultData = ['session' => $sessionCode, 'post_id' => $id];

        Jsonponse::success('sync success', $resultData, 202);
    } // END function


    /**
     * make2Html
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function makePostAll2Html(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $status = config('tbl_posts.POSTS_STATUS_PUBLISH');
        $postData = $this->postServ->findByStatus($status);

        if ($postData->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $successPostIds = $failPostIds = [];
        foreach ($postData->all() as $postDatum) {

            $categoryDatum = $this->categoryServ->findById($postDatum->category_id);
            $categoryDatum->first()->url_path = '/' . $categoryDatum->first()->slug . '-c' . $categoryDatum->first()->id;

            $categoryDatum->first()->cover_links = [];
            if (!empty($categoryDatum->first()->cover)) {
                $coverLinkData = $this->fileServ->findLinks($categoryDatum->first()->cover);
                $categoryDatum->first()->cover_links = $coverLinkData;
            } // END if else

            $postDatum->category = get_object_vars($categoryDatum->first());


            $authorDatum = $this->authorServ->findById($postDatum->author_id);
            $authorDatum->first()->avatar_links = [];
            if (!empty($authorDatum->first()->avatar)) {
                $avayarLinkData = $this->fileServ->findLinks($authorDatum->first()->avatar);
                $authorDatum->first()->avatar_links = $avayarLinkData;
            } // END if else

            $postDatum->author = get_object_vars($authorDatum->first());


            $postDatum->url_path = '/' . $postDatum->slug . '-p' . $postDatum->id;
            $postDatum->site_name = 'English.Agency';
            $postDatum->cover_links = [];
            if (!empty($postDatum->cover)) {
                $coverLinkData = $this->fileServ->findLinks($postDatum->cover);
                $postDatum->cover_links = $coverLinkData;
            } // END if else

            if (empty($postDatum->canonical_url)) {
                $postDatum->canonical_url = env('WEB_URL') . $postDatum->url_path;
            } // END if


            $tHtmlPath = 'post/' . $postDatum->status . '/';
            $htmlFile  = $postDatum->id . '.html';

            $tExists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);
            if (!empty($tExists)) {
                Storage::disk('public')->delete($tHtmlPath . $htmlFile);
            } // END if


            $inputData = get_object_vars($postDatum);
            $content = view('post', $inputData)->__toString();

            Storage::disk('public')->put($tHtmlPath . $htmlFile, $content);


            $exists = Storage::disk('public')->exists($tHtmlPath . $htmlFile);

            if (empty($exists)) {
                $failPostIds[] = $postDatum->id;
            } else {
                $this->postServ->update(['html_made' => true], ['id' => $postDatum->id]);
                $successPostIds[] = $postDatum->id;
            } // END if
        } // END foreach


        if (!empty($failPostIds)) {
            $code = 404;
            $comment = 'html file error';
            $data['fail_post_ids'] = $failPostIds;

            Jsonponse::fail($comment, $code, $data);
        } // END if


        $resultData = ['session' => $sessionCode, 'success_post_ids' => $successPostIds];

        Jsonponse::success('make success', $resultData, 202);
    } // END function


    /**
     * syncHtml
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function syncPostAllHtml(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $postData = $this->postServ->findAll([], -1, 10, true);

        if ($postData->isEmpty()) {
            $code = 404;
            $comment = 'post error';

            Jsonponse::fail($comment, $code);
        } // END if


        $successPostIds = $failPostIds = [];
        foreach ($postData->all() as $postDatum) {

            $oHtmlPath = 'public/post/' . $postDatum->status . '/';
            $tHtmlPath = 'sync/post/';
            $htmlFile = $postDatum->id . '.html';


            if ($postDatum->status == config('tbl_posts.POSTS_STATUS_PUBLISH')) {

                if (empty($postDatum->html_made)) {
                    $failPostIds[] = $postDatum->id;
                } else {

                    $oExists = Storage::disk('local')->exists($oHtmlPath . $htmlFile);
                    if (!empty($oExists)) {
                        $tExists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);
                        if (!empty($tExists)) {
                            Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                        } // END if

                        Storage::disk('local')->copy($oHtmlPath . $htmlFile, $tHtmlPath . $htmlFile);
                    } // END if
                } // END if
            } // END if

            if ($postDatum->status != config('tbl_posts.POSTS_STATUS_PUBLISH')) {

                $exists = Storage::disk('local')->exists($tHtmlPath . $htmlFile);

                if (!empty($exists)) {
                    Storage::disk('local')->delete($tHtmlPath . $htmlFile);
                } // END if
            } // END if

            if (!empty($postDatum->html_made)) {
                $this->postServ->update(['synced' => true], ['id' => $postDatum->id]);
                $successPostIds[] = $postDatum->id;
            } // END if
        } // END foreach


        if (!empty($failPostIds)) {
            $code = 422;
            $comment = 'html_made error';
            $data['fail_post_ids'] = $failPostIds;

            Jsonponse::fail($comment, $code, $data);
        } // END if


        $this->_writeData2Htaccess();


        $resultData = ['session' => $sessionCode, 'success_post_ids' => $successPostIds];

        Jsonponse::success('sync success', $resultData, 202);
    } // END function


    /**
     * make2Html
     *
     * @method PUT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @throws
     * @return
     */
    public function makeHtaccess(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session empty';

            Jsonponse::fail($comment, $code);
        } // END if


        $isAlive = $this->sessionServ->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 410;
            $comment = 'session is NOT alive';

            Jsonponse::fail($comment, $code);
        } // END if


        $this->_writeData2Htaccess();


        $resultData = ['session' => $sessionCode];

        Jsonponse::success('make success', $resultData, 202);
    } // END function


    private function _writeData2Htaccess()
    {


        $redirects = [];

        $postData = $this->postServ->findByStatus('publish');

        if ($postData->isNotEmpty()) {
            foreach ($postData->all() as $postDatum) {
                $postDatum->slug = str_replace([" ",".html"], "", trim($postDatum->slug));
                $redirects['post'][$postDatum->id] = $postDatum->slug;
            }
        } // END if


        $categoryData = $this->categoryServ->findAll('enable');

        if ($categoryData->isNotEmpty()) {
            foreach ($categoryData->all() as $categoryDatum) {
                $categoryDatum->slug = str_replace([" ",".html"], "", trim($categoryDatum->slug));
                $redirects['category'][$categoryDatum->id] = $categoryDatum->slug;
            }
        } // END if


        if (!empty($redirects)) {

            $tHtmlPath = 'sync/';
            $htmlFile  = 'htaccess';
            $content = view('htaccess', $redirects)->__toString();

            Storage::disk('local')->put($tHtmlPath . $htmlFile, $content);
        } // END if

    } // END function


} // END class
