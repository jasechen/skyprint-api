<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// ===
Route::pattern('id', '[0-9]+');

Route::group(['prefix' => 'file'], function () use ($router) {
    $router->post('', 'File@upload');
});


Route::group(['prefix' => 'log'], function () use ($router) {
    $router->post('click',     'Log@createClick');
    $router->post('view/post', 'Log@createViewPost');
});


Route::group(['prefix' => 'session'], function () use ($router) {
    $router->post('init',  'Session@init');
    $router->get('{code}', 'Session@findByCode');
});




