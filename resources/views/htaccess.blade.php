
@foreach ($category as $id => $slug)
Redirect 301 /{{$slug}} /{{$slug}}-c{{$id}}
@endforeach

@foreach ($post as $id => $slug)
Redirect 301 /{{$slug}} /{{$slug}}-p{{$id}}
@endforeach

Redirect 301 /about-us/service-policy /service-policy-g20002
Redirect 301 /about-us/privacy-policy /privacy-policy-g20003
Redirect 301 /about-us /about-us-g20001
Redirect 301 /join-us/家教合作申請 /家教合作申請-g20005
Redirect 301 /join-us/教育顧問合作申請 /教育顧問合作申請-g20006
Redirect 301 /join-us/媒體合作申請 /媒體合作申請-g20007

<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    RewriteEngine On

    RewriteRule ^(.*)-c(\d+)$ /sync/category/$2.html [L]
    RewriteRule ^(.*)-p(\d+)$ /sync/post/$2.html [L]
    RewriteRule ^(.*)-g(\d+)$ /guide/$2.html [L]

</IfModule>