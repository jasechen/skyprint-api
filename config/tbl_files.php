<?php
$fields = [
    'FILES_TYPE_IMAGE' => 'image',
    'FILES_TYPE_VIDEO' => 'video',
    'FILES_TYPE_AUDIO' => 'audio',
    'FILES_TYPE_OTHERS' => 'others',

    'FILES_STATUS_DELETE'  => 'delete',
    'FILES_STATUS_UPLOAD'  => 'upload',
    'FILES_STATUS_PUSH'  => 'push',
    'FILES_STATUS_PULL'  => 'pull',
    'FILES_STATUS_FINISH'  => 'finish',
    'FILES_STATUS_PRINT'  => 'print',
];

$fields['DEFAULT_FILES_TYPE'] = $fields['FILES_TYPE_IMAGE'];
$fields['FILES_TYPES'] = [
    $fields['FILES_TYPE_IMAGE'],
    $fields['FILES_TYPE_VIDEO'],
    $fields['FILES_TYPE_AUDIO'],
    $fields['FILES_TYPE_OTHERS']
];

$fields['DEFAULT_FILES_STATUS'] = $fields['FILES_STATUS_UPLOAD'];
$fields['FILES_STATUS'] = [
    $fields['FILES_STATUS_DELETE'],
    $fields['FILES_STATUS_UPLOAD'],
    $fields['FILES_STATUS_PUSH'],
    $fields['FILES_STATUS_PULL'],
    $fields['FILES_STATUS_FINISH'],
    $fields['FILES_STATUS_PRINT']
];

return $fields;