<?php

namespace Package\Litedbquery;

use Package\Litedbquery\Litedbquery;
use Illuminate\Support\ServiceProvider;

class LitedbqueryServiceProvider extends ServiceProvider
{

    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->singleton(Litedbquery::class, function () {
            return new Litedbquery();
        });
    }

}